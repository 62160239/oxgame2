/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.oxgame;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class TicTacTo {

    static Scanner kb = new Scanner(System.in);
    static int row, col;
    static boolean isFinish = false;
    static char winner = '-';

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Row and Column is not empty.");
        }
    }

    static void checkColumn() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkDiagonal() {
        if (table[0][0] == table[1][1]
                && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            isFinish = true;
            winner = player;
            return;
        }
        if (table[0][2] == table[1][1]
                && table[1][1] == table[2][0]
                && table[0][2] != '-') {
            isFinish = true;
            winner = player;
            return;
        }
    }

    static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        isFinish = true;
        return true;
    }

    static void checkWin() {
        checkRow();
        checkColumn();
        checkDiagonal();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!!");
            isFinish = true;
        } else {
            System.out.println("Player " + winner + " Win!!!");
            isFinish = true;
        }
    }

    static void showBye() {
        System.out.println("Bye Bye....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
